<?php
namespace AllProducts;

use Product\Product;

class AllProducts extends Product
{
    /**
     * Find all products from the database
     *
     * @return void
     */
    public function createProduct()
    {
        global $database;
        $result_set = $database->query("SELECT * FROM product");
        return $result_set;
    }
}
