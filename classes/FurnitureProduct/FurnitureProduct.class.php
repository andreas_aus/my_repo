<?php
namespace FurnitureProduct;

use Product\Product;

class FurnitureProduct extends Product
{   
    /**
     * Insert furniture product into database
     *
     * @return void
     */
    public function createProduct()
    {
        global $database;
        $sql = "INSERT INTO product (sku, name, price, height, width, length) VALUES ";
        $sql .= "('$this->sku', '$this->name', '$this->price','$this->height', '$this->width', '$this->length')";

        if ($database->query($sql)) {
            return true;
        } else {
            var_dump($sql);
            return false;
        }
    }
}
