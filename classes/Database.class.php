<?php
namespace Database;
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/config.php";
class Database
{
    /**
     * Connection to the database
     *
     * @param object
     */
    private $connection;

    /**
     * Open database connection automatically
     */
    public function __construct()
    {
        $this->openDbConnection();
    }
    /**
     * Open connection to mysql database
     *
     * @return object
     */
    private function openDbConnection()
    {
        $this->connection = new \mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if ($this->connection->connect_errno) {
            die("Database connection has failed " . $this->connection->connect_error);
        }
    }
    /**
     * Query the database
     *
     * @param [type] $sql
     * @return void
     */
    public function query($sql)
    {
        $result = $this->connection->query($sql);

        $this->confirmQuery($result);

        return $result;
    }
    /**
     * Check if query is returning anything
     *
     * @param [type] $result
     * @return void
     */
    private function confirmQuery($result)
    {
        if (!$result) {
            die("Query Failed" . $this->connection->error);
        }
    }
    /**
     * Escape the string to protect against sql injection
     *
     * @param [type] $string
     * @return void
     */
    public function escapeString($string)
    {
        $escaped_string = $this->connection->real_escape_string($string);

        return $escaped_string;
    }
}

$database = new Database();
