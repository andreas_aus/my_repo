<?php
namespace BookProduct;

use Product\Product;

class BookProduct extends Product
{   
    /**
     * Insert book product into database
     *
     * @return void
     */
    public function createProduct()
    {
        global $database;
        $sql = "INSERT INTO product (sku, name, price,weight) VALUES ";
        $sql .= "('$this->sku','$this->name', '$this->price', '$this->weight')";
        if ($database->query($sql)) {
            return true;
        } else {
            var_dump($sql);
            return false;
        }
    }
}
