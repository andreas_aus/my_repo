<?php
namespace Product;

abstract class Product
{
    /**
     * SKU for product
     *
     * @var string
     */
    protected $sku;
    /**
     * Name of product
     *
     * @var string
     */
    protected $name;
    /**
     * Price of product
     *
     * @var int
     */
    protected $price;
    /**
     * Size of product
     *
     * @var int
     */
    protected $size;
    /**
     * Height of product
     *
     * @var string
     */
    protected $height;
    /**
     * Width of product
     *
     * @var string
     */
    protected $width;
    /**
     * Length of product
     *
     * @var string
     */
    protected $length;
    /**
     * Weight of product
     *
     * @var string
     */
    protected $weight;
    /**
     * Add new product to database
     *
     * @return void
     */
    abstract function createProduct();
    /**
     * Set values to properties
     *
     * @return void
     */
    public function setInputValues()
    {
        global $database;
        isset($_POST['sku']) ? $this->sku = $database->escapeString($_POST['sku']) : null;
        isset($_POST['name']) ? $this->name = $database->escapeString($_POST['name']) : null;
        isset($_POST['price']) ? $this->price = $database->escapeString($_POST['price']) : null;
        isset($_POST['size']) ? $this->size = $database->escapeString($_POST['size']) : null;
        isset($_POST['weight']) ? $this->weight = $database->escapeString($_POST['weight']) : null;
        isset($_POST['height']) ? $this->height = $database->escapeString($_POST['height']) : null;
        isset($_POST['width']) ? $this->width = $database->escapeString($_POST['width']) : null;
        isset($_POST['length']) ? $this->length = $database->escapeString($_POST['length']) : null;
    }
    /**
     * Redirect user to another page
     *
     * @param string
     * @return void
     */
    public function redirect($location)
    {
        header("Location: {$location}");
    }
}
