<?php
namespace DvdProduct;

use Product\Product;

class DvdProduct extends Product
{   
    /**
     * Insert dvd product into database
     *
     * @return void
     */
    public function createProduct()
    {
        global $database;
        $sql = "INSERT INTO product (sku, name, price, size) VALUES ";
        $sql .= "('$this->sku','$this->name','$this->price','$this->size')";

        if ($database->query($sql)) {
            return true;
        } else {
            var_dump($sql);
            return false;
        }
    }
}
