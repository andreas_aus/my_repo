<?php ob_start(); ?>
<?php require $_SERVER['DOCUMENT_ROOT'] . "/includes/init.php" ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Scandiweb Product List App</title>
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="/sass/styles.css">
    <link rel="icon" href="/images/favicon-32x32.png" sizes="32x32" type="image/png"> 
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    
  </head>
  <body class="app">
    <header class="app__header">
      <div>
        <a href="index.php">
          <img
            class="app__headerLogo"
            src="/images/scandiweb_logo.webp"
            alt="logo"
          />
        </a>
      </div>
      <div>
        <a href="add_product.php"><button class="app__headerBtn1">ADD</button></a>
      
        <button 
        class="app__headerBtn2" 
        name="delete" 
        type="submit"
        form="mass-delete"
        >MASS DELETE</button>
      </div>
    </header>