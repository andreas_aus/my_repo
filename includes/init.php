<?php 
require $_SERVER['DOCUMENT_ROOT'] . "/classes/database.class.php";

spl_autoload_register(function ($class){
require $_SERVER['DOCUMENT_ROOT'] ."/classes/" . str_replace('\\', '/', $class) . '.class.php';
});
?>