<?php
use DvdProduct\DvdProduct;
use BookProduct\BookProduct;
use FurnitureProduct\FurnitureProduct;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	switch ($_POST) {
		case isset($_POST['submit_dvd']):
				$product = new DvdProduct();
				$product->setInputValues();
				$product->createProduct();
				$product->redirect("index.php");  
			break;
		case isset($_POST['submit_book']):
		  $product = new BookProduct();
				$product->setInputValues();
				$product->createProduct();
				$product->redirect("index.php");
			break;
		case isset($_POST['submit_furniture']):
 	  $product = new FurnitureProduct();
				$product->setInputValues();
				$product->createProduct();
				$product->redirect("index.php");
			break;
		default:
			break;
	}
}
?>

<section class="form__container">
   <form id="form__addProduct" class="form__submit" action="" method="POST">
      <h1 class="form__heading">Add new product</h1>
      <div class="form__inputContainer">

									<h1>SKU</h1>
         <label for="sku"></label>
         <input 
            id="form__inputSku"
            type="text" 
            name="sku" 
            value="<?php if(isset($_POST['sku'])) echo $_POST['sku']; ?>" 
            autofocus >

									<h1>Name</h1>
         <label for="name"></label>
         <input 
            id="form__inputName"
            type="text" 
            name="name" 
            value="<?php if(isset($_POST['name'])) echo $_POST['name']; ?>">

									<h1>Price</h1>
         <label for="price"></label>
         <input 
            id="form__inputPrice"
            type="text" 
            name="price" 
            value="<?php if(isset($_POST['price'])) echo $_POST['price']; ?>">

         <p class="form__type">Type</p>
         <select id="select_type" required>
            <option value="" selected>Select</option>
            <option value="dvd">Dvd</option>
            <option value="book" >Book</option>
            <option value="furniture">Furniture</option>
         </select>
         <div class="form__productDesc"></div>

         <div class="form__btns">
            <button id="form__submitDefault" class="form__btn1">Save</button>
            <a href="index.php">
            <button class="form__btn2" type="button">Cancel</button>
            </a>
         </div>
      </div>
   </form>
</section>

<script>
// Change product input fields on select
$("#select_type").change(function() {
	// Target select value
	var display_type = $("#select_type option:selected").text();
	// Dvd product description box  
	var dvd_desc = `
   <div class='form__productDesc'>
				<h1>Size</h1>
				 <label for='sku'></label>
				<input type='text' id='form__inputSize' value='<?php if(isset($_POST['size'])) echo $_POST['size']; ?>' name='size' />
   </div>
   `;
	// Furniture product description box 
	var furniture_desc = `
   <div class='form__productDesc'>
	<p>Height</p>
	<input id='form__inputHeight' type='text' value='<?php if(isset($_POST['height'])) echo $_POST['height']; ?>' name='height' />
	<p>Width</p>
	<input id='form__inputWidth' type='text' value='<?php if(isset($_POST['width'])) echo $_POST['width']; ?>' name='width' />
	<p>Length</p>
	<input id='form__inputLength' type='text' value='<?php if(isset($_POST['length'])) echo $_POST['length']; ?>' name='length' />
   </div>
   `;
	// Book product description box 
	var book_desc = `
   <div class='form__productDesc'>
			<p>Weight</p>
			
			<input id='form__inputWeight' type='text' value='<?php if(isset($_POST['weight'])) echo $_POST['weight']; ?>' name='weight' />
   </div>
   `;
	// Dvd product submit button
	var dvd_btn = "<button id='submit__dvdBtn' class='form__btn1' type='submit' name='submit_dvd'>Save</button>";
	// Furniture product submit for button
	var furniture_btn = "<button id='submit_furnitureBtn' class='form__btn1' type='submit' name='submit_furniture'>Save</button>";
	// Book product submit button
	var book_btn = "<button id='submit_bookBtn' class='form__btn1' type='submit' name='submit_book'>Save</button>";
	// Target sku input field
	var sku = $("#form__inputSku").val();
// Error messages
	var data = "Please, submit required data";
	var type = "Please, provide the data of indicated type";
	var duplicate = "Please, provide unique SKU";
	// Switch product type
switch (display_type) {
	case "Dvd":
		$("div.form__productDesc")
			.replaceWith(dvd_desc);
		$("button.form__btn1")
			.replaceWith(dvd_btn);
// Validate dvd product
		$(document).on("mouseenter", "#submit__dvdBtn", function() {
	$("#form__addProduct").validate({
		rules: {
			sku: {
				required: true,
				digits: true,
				minlength: 1,
				remote: {
					url: "includes/duplicate_check.php",
					type: "post",
					dataType: "json",
					data: {
						sku: function() {
							return $("#form__inputSku").val();
						}
					}
				}
			},
			name: {
				required: true,
				minlength: 1
			},
			price: {
				required: true,
				minlength: 1,
				digits: true
			},
			size: {
				required: true,
				minlength: 1,
				digits: true
			}
		},
		messages: {
			sku: {
				required: data,
				digits: type,
				remote: duplicate
			},
			name: {
				required: data,
			},
			price: {
				required: data,
				digits: type
			},
			size: {
				required: data,
				digits: type
			}
		}
	});
		$("#form__addProduct").invalid();
	if ($("#form__addProduct").valid()) {
		$("#submit__dvdBtn").submit();
	}
});
		break;
	case "Book":
		$("div.form__productDesc")
			.replaceWith(book_desc);
		$("button.form__btn1")
			.replaceWith(book_btn);
			// Validate book product
		$(document).on("mouseenter", "#submit_bookBtn", function() {
	$("#form__addProduct").validate({
		rules: {
			sku: {
				required: true,
				digits: true,
				minlength: 1,
				remote: {
					url: "includes/duplicate_check.php",
					type: "post",
					dataType: "json",
					data: {
						sku: function() {
							return $("#form__inputSku").val();
						}
					}
				}
			},
			name: {
				required: true,
				minlength: 1
			},
			price: {
				required: true,
				minlength: 1,
				digits: true
			},
			weight: {
				required: true,
				minlength: 1,
				digits: true
			}
		},
		messages: {
			sku: {
				required: data,
				digits: type,
				remote: duplicate
			},
			name: {
				required: data,
			},
			price: {
				required: data,
				digits: type
			},
			weight: {
				required: data,
				digits: type
			}
		}
	});
		$("#form__addProduct").invalid();
	if ($("#form__addProduct").valid()) {
		$("#submit_bookBtn").submit();
	}
});
		break;
	case "Furniture":
		$("div.form__productDesc")
			.replaceWith(furniture_desc);
		$("button.form__btn1")
			.replaceWith(furniture_btn);
				// Validate furniture product
		$(document).on("mouseenter", "#submit_furnitureBtn", function() {
	$("#form__addProduct").validate({
		rules: {
			sku: {
				required: true,
				digits: true,
				minlength: 1,
				remote: {
					url: "includes/duplicate_check.php",
					type: "post",
					dataType: "json",
					data: {
						sku: function() {
							return $("#form__inputSku").val();
						}
					}
				}
			},
			name: {
				required: true,
				minlength: 1
			},
			price: {
				required: true,
				minlength: 1,
				digits: true
			},
			height: {
				required: true,
				minlength: 1,
				digits: true
			},
			width: {
				required: true,
				minlength: 1,
				digits: true
			},
			length: {
				required: true,
				minlength: 1,
				digits: true
			}
		},
		messages: {
			sku: {
				required: data,
				digits: type,
				remote: duplicate
			},
			name: {
				required: data,
			},
			price: {
				required: data,
				digits: type
			},
			height: {
				required: data,
				digits: type
			},
			width: {
				required: data,
				digits: type
			},
			length: {
				required: data,
				digits: type
			}
		}
	});
		$("#form__addProduct").invalid();
	if ($("#form__addProduct").valid()) {
		$("#submit_furnitureBtn").submit();
	}
});
		break;
	default:
		break;
}
});
</script>

