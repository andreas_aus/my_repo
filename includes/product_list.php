<?php
use AllProducts\AllProducts;
$product = new AllProducts();
$result_set = $product->createProduct();

if (isset($_POST['delete'])) {
    $count = array();
    $count = count($_POST['checkbox']);
    for ($i = 0; $i < $count; $i++) {

        $delete_sku = $_POST['checkbox'][$i];
        $sql = "DELETE FROM product WHERE sku=" . $delete_sku;
        $database->query($sql);
    }
    $product->redirect("index.php");
}
?>

<section class="app__productList">
    <form id="mass-delete" action="" method="POST" class="app__productContainer">
      <?php while ($row = mysqli_fetch_array($result_set)): ?>
      <?php $formatted_price =  number_format($row['price'], 2, '.', '')?>
      <div class="app__singleProductContainer">
          <input
            type="checkbox"
            class="app__singleProductCheckbox"
            name="checkbox[]"
            value="<?php echo $row['sku'] ?>"
            />
          <div class="app__singleProductDetails">
          <?php echo $row['sku'] ? "<p>SKU: {$row['sku']}</p>" : null ?>
          <?php echo $row['name'] ? "<p>Name: {$row['name']}</p>" : null ?>
          <?php echo $row['price'] ? "<p>Price: {$formatted_price}</p>" : null ?>
          <?php echo $row['size'] ? "<p>Size: {$row['size']}</p>" : null ?>
          <?php echo $row['height'] ? "<p>Height: {$row['height']}</p>" : null ?>
          <?php echo $row['width'] ? "<p>Width: {$row['width']}</p>" : null ?>
          <?php echo $row['length'] ? "<p>Length: {$row['length']}</p>" : null ?>
          <?php echo $row['weight'] ? "<p>Weight: {$row['weight']}</p>" : null ?>
          </div>
      </div>
      <?php endwhile;?>
    </form>
    </div>
</section>

